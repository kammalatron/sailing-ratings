async function renderSettings() {
    const settings = await getSettings();
    console.log(settings);

    let allowOthers = document.getElementById('setting-allow-others');
    allowOthers.onclick = setAllowOthers;
    allowOthers.checked = settings.allowOthers;

    let resetUserButton = document.getElementById('setting-reset-user');
    resetUserButton.onclick = resetUser;

    let currentUserField = document.getElementById('setting-tracked-user');
    currentUserField.value = settings.currentUser;

    let showMarkers = document.getElementById('setting-show-markers');
    showMarkers.onclick = setLogMarkers;
    showMarkers.checked = settings.logMarkers.show;

    let sentLogsMarkers = document.getElementById('setting-hide-sent-logs-markers');
    sentLogsMarkers.onclick = hideSentLogMarkers;
    sentLogsMarkers.checked = !settings.logMarkers.showSent;
    sentLogsMarkers.disabled = !settings.logMarkers.show;

    let keepLastLogs = document.getElementById('setting-keep-last-logs');
    keepLastLogs.onchange = setKeepLastLogs;
    keepLastLogs.value = settings.keepLastLogs;

    let clearLogsButton = document.getElementById('setting-clear-logs');
    clearLogsButton.onclick = clearLogs;
    clearLogsButton.disabled = Object.entries(settings.storedLogs).length === 0;

    renderLogStats(settings.storedLogs)
}


async function getSettings() {
    return await chrome.storage.local.get({
        allowOthers: false,
        currentUser: null,
        logMarkers: {show: true, showSent: true},
        keepLastLogs: 100,
        storedLogs: {},
    });
}

async function setAllowOthers() {
    const newSettings = {
        allowOthers: document.getElementById('setting-allow-others').checked,
    };
    await chrome.storage.local.set(newSettings);
}

async function setLogMarkers() {
    const show = document.getElementById('setting-show-markers').checked;

    const currentSettings = await getSettings();

    const newSettings = {
        logMarkers: {
            show: show,
            showSent: currentSettings.logMarkers.showSent,
        },
    };
    await chrome.storage.local.set(newSettings);

    const hideSent = document.getElementById('setting-hide-sent-logs-markers');
    hideSent.disabled = !newSettings.logMarkers.show;
}

async function hideSentLogMarkers() {
    const hideMarkers = document.getElementById('setting-hide-sent-logs-markers').checked;

    const currentSettings = await getSettings();

    const newSettings = {
        logMarkers: {
            show: currentSettings.logMarkers.show,
            showSent: !hideMarkers,
        },
    };
    await chrome.storage.local.set(newSettings);
}

async function resetUser() {
    let currentUserField = document.getElementById('setting-tracked-user');
    currentUserField.value = "";
    const newSettings = {currentUser: null};
    await chrome.storage.local.set(newSettings);
}

async function setKeepLastLogs() {
    const keepLastLogs = document.getElementById('setting-keep-last-logs').value;

    await chrome.storage.local.set({keepLastLogs: parseInt(keepLastLogs)});
}

async function clearLogs() {
    const currentSettings = await getSettings();
    if (currentSettings.keepLastLogs === 0) {
        await chrome.storage.local.set({storedLogs: {}});
        return;
    }

    const shortenedLogs = Object.entries(currentSettings.storedLogs).map(([key, value]) => [key, value.slice(0, currentSettings.keepLastLogs)]);

    await chrome.storage.local.set({storedLogs: Object.fromEntries(shortenedLogs)});
}

function renderLogStats(logs) {
    const stats = document.getElementById('log-stats-container');
    stats.innerHTML = '';

    if (Object.keys(logs).length === 0) {
        const noLogsMessage = document.createElement('div');
        noLogsMessage.classList.add('empty-block-message');
        noLogsMessage.innerHTML = '&mdash;&nbsp;нет логов&nbsp;&mdash;';
        stats.appendChild(noLogsMessage);
        return;
    }

    for (const [user, userLogs] of Object.entries(logs)) {
        const userStatsRow = document.createElement('div');
        userStatsRow.classList.add('log-stats-row');

        const userName = document.createElement('span');
        userName.classList.add('log-stats-name');
        userName.innerText = user;
        userStatsRow.appendChild(userName);

        const logsCount = document.createElement('span');
        logsCount.innerText = userLogs.length;
        userStatsRow.appendChild(logsCount);

        stats.appendChild(userStatsRow);
    }
}

renderSettings()
