function getMarkerNode(currentLog, logs, showSent) {
    if (logs.includes(currentLog) && !showSent) {
        return null;
    }

    const elem = document.createElement("span");
    elem.classList.add("sailing-ratings-marker");
    elem.style.position = "absolute";
    elem.style.right = 0;

    if (logs.includes(currentLog)) {
        elem.setAttribute("title", "лог уже был отправлен в рейтинги")
        elem.append("⛵");
        elem.append("✅");
        return elem;
    }

    elem.setAttribute("title", "лог не был отправлен в рейтинги")
    elem.append("⛵");
    elem.append("❌");

    return elem;
}


const body = document.querySelector('body')

let chroniclesObserver = new MutationObserver(async (mutationList) => {
    let {logMarkers, currentUser, storedLogs} = await chrome.storage.local.get({
        logMarkers: {show: true, showSent: true},
        currentUser: null,
        storedLogs: Object.fromEntries(new Map()),
    });

    if (currentUser === null || !logMarkers.show) return;

    storedLogs = new Map(Object.entries(storedLogs));

    for (const mutation of mutationList) {
        for (const addedNode of mutation.addedNodes) {
            if (!addedNode.classList.contains("wl_line")) {
                continue;
            }

            const link = addedNode.querySelector(":scope a");
            if (link?.innerText !== "Заплыв") continue;

            const elem = getMarkerNode(
                link.href,
                storedLogs.get(currentUser) ?? [],
                logMarkers.showSent,
            );
            if (elem === null) continue;
            addedNode.querySelector(':scope .wl_ftype').appendChild(elem);
        }
    }
})

let bodyObserver = new MutationObserver((mutationList) => {
    for (const mutation of mutationList) {
        for (const addedNode of mutation.addedNodes) {
            if (addedNode.id !== "wup0") continue;

            chroniclesObserver.observe(addedNode, {childList: true, subtree: true})
        }

        for (const removedNode of mutation.removedNodes) {
            if (removedNode.id !== "wup0") continue;

            chroniclesObserver.disconnect()
        }
    }
})


bodyObserver.observe(body, {childList: true});
