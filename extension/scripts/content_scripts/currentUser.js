async function setCurrentUser(attempt) {
    const {currentUser} = await chrome.storage.local.get({currentUser: null});
    if (currentUser) {
        console.log(`already tracking ${currentUser}`);
        return
    }

    if (!document.querySelector('#hk_name a')) {
        console.log("no user name to define. are you in duel?");
        const delay = attempt === 0 ? 1000 : 1000 * 60;
        setTimeout(() => setCurrentUser(attempt + 1), delay);
        return
    }
    const godName = decodeURI(document.querySelector('#hk_name a').href).substring("https://godville.net/gods/".length);
    console.log(`start tracking user ${godName}`);
    await chrome.storage.local.set({currentUser: godName});
}

setCurrentUser(0);
