function addMarkers(logs, showSent) {
    for (const cell of document.querySelectorAll("#wrap td:has(a)")) {
        const link = cell.querySelector(":scope a");
        if (link.innerText !== "Заплыв") continue;

        if (logs.includes(link.href) && !showSent) {
            continue;
        }

        const elem = document.createElement("span");
        elem.classList.add("sailing-ratings-marker")
        cell.appendChild(elem)

        if (logs.includes(link.href)) {
            elem.setAttribute("title", "лог уже был отправлен в рейтинги")
            elem.append("⛵");
            elem.append("✅");
            continue;
        }

        elem.setAttribute("title", "лог не был отправлен в рейтинги")
        elem.append("⛵");
        elem.append("❌");
    }
}

async function render() {
    let {logMarkers, currentUser, storedLogs} = await chrome.storage.local.get({
        logMarkers: {show: true, showSent: true},
        currentUser: null,
        storedLogs: Object.fromEntries(new Map()),
    });

    if (currentUser === null || !logMarkers.show) return;

    storedLogs = new Map(Object.entries(storedLogs));

    addMarkers(storedLogs.get(currentUser) ?? [], logMarkers.showSent);
}

render();
