function getLogDate() {
    const [logDate, logTime] = document.querySelector('.ft').innerText.split(' ');
    const [day, month, year] = logDate.split('.');
    const [hours, mins] = logTime.split(':')
    return new Date(parseInt(year), parseInt(month) - 1, parseInt(day), parseInt(hours), parseInt(mins));
}

function formatDate(dateObj) {
    return dateObj.toLocaleDateString('sv', {timeZone: "Europe/Moscow"});
}

async function renderStats() {
    if (document.querySelector('.lastduelpl').innerText !== 'Заплыв') {
        console.log("not a sailing log. ignored.");
        return;
    }

    let settings = await chrome.storage.local.get({
        allowOthers: false, currentUser: null,
    });

    if (settings.currentUser === null && !settings.allowOthers) {
        console.log("no user selected and others are not allowed");
        return;
    }

    for (const sailer of document.getElementById("h_tbl").children) {
        const godname = sailer.querySelector(".c1 a").innerText;

        if (settings.currentUser !== godname && !settings.allowOthers) {
            continue;
        }
        const id = Array.from(sailer.classList).filter(classname => classname.startsWith("saild_"))[0];

        const lastStep = document.querySelector(`.${id}.d_imp`).getAttribute('data-t')
        const finalScore = sailer.querySelector(".c2 .cg").innerText;
        const finalBoxes = Array.from(finalScore).filter((s => s === '📦')).length;
        const finalBigPrices = Array.from(finalScore).filter((s => s === '♂' || s === '♀' || s === '💰')).length;

        const logDate = getLogDate();

        let logURL = new URL(window.location.href);
        logURL.search = "";
        logURL = logURL.toString();

        const report = document.createElement('button');
        report.setAttribute("title", "отправить в рейтинги")
        report.append("⛵");
        sailer.appendChild(report);


        report.onclick = async () => {
            let {storedLogs, keepLastLogs} = await chrome.storage.local.get({
                storedLogs: {},
                keepLastLogs: 100
            });
            storedLogs = new Map(Object.entries(storedLogs));

            const userStoredLogs = storedLogs.has(godname) ? storedLogs.get(godname) : [];

            if (userStoredLogs.includes(logURL)) {
                const result = window.confirm("мы уже отправляли этот лог. точно отправить ещё раз?");
                if (!result) {
                    return;
                }
            }
            if (!userStoredLogs.includes(logURL)) {
                userStoredLogs.push(logURL);
                if (userStoredLogs.length >= keepLastLogs) {
                    userStoredLogs.splice(0, userStoredLogs.length - keepLastLogs)
                }
                storedLogs.set(godname, userStoredLogs);
                await chrome.storage.local.set({storedLogs: Object.fromEntries(storedLogs)});
            }

            const formUrl = new URL("https://docs.google.com/forms/d/e/1FAIpQLSfmNsuUW4N1_fk-wFAqeHCofYtEECU7dul3zmRgOObIAMWUzA/viewform")
            const params = new URLSearchParams();
            params.append("usp", "pp_url");
            params.append("entry.813089936", logURL);
            params.append("entry.724591067", godname);
            params.append("entry.1101315130", formatDate(logDate));
            params.append("entry.1952458026", finalBoxes);
            params.append("entry.1801854224", finalBigPrices);
            // params.append("entry.671465567", 0);
            params.append("entry.1168887957", lastStep);

            formUrl.search = params.toString();
            window.open(formUrl);
        }
    }
}

renderStats()
